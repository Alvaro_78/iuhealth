class Bus {
  constructor() {
    this.actions = {}
  }

  subscribe(topic, action){
    if (!this._existsActions(topic)) {
        this._initializeActions(topic)
    }

    this._addAction(topic, action)
  }

  publish(topic, message){
    if (this._existsActions(topic)) {
        this._executeActions(topic, message)
    }
  }

  _existsActions(topic) {
    return (this.actions[topic] != undefined)
  }

  _executeActions(topic, message) {
    this.actions[topic].forEach((action) => {
        action(message)
    })
  }

  _initializeActions(topic) {
    this.actions[topic] = []
  }

  _addAction(topic, action) {
    this.actions[topic].push(action)
  }
}

export default Bus