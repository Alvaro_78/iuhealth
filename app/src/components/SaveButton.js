import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

const template = document.createElement('template')

template.innerHTML = `
  <style>
    div { margin-top: 15px}
  </style>

  <div class="container">
    <button id='save'>Save</button>
  </div>
`

export default class SaveButton extends Component {
  constructor() {
    super(template, myBus)
    
    let button_element = this.shadowroot.querySelector('#save')

    button_element.addEventListener('click', () => {
      this.bus.publish('save.button.clicked')
    })

  }

}
window.customElements.define('save-button', SaveButton)