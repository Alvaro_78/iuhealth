import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

import NameInput from './NameInput'
import AlertBox from './AlertBox'
import SaveButton from './SaveButton'

const template = document.createElement('template')

template.innerHTML = `
  <h1 id='title'>Aplication title</h1>
  <name-input></name-input>
  <alert-box></alert-box>
  <save-button></save-button>
`

export default class App extends Component {
  constructor() {
    super(template, myBus)
    this.state = {}
    
    this.bus.subscribe('name.changed', name => this.state.name = name)
    this.bus.subscribe('save.button.clicked', () => this.trySave())
  }

  showAlert() {
    let alert_message = 'No puedes salvar un nombre vacío'
    this.bus.publish('validation.error', alert_message)
  }

  trySave(){
    if (!this.state.name) {
      this.showAlert()
      return
    }
    this.save()
  }

  save() {
    console.log(`Saving state:`, this.state)
  }
  
  static get observedAttributes() {
    return ['title']
  }
  
  attributeChangedCallback() {
    this.shadowroot.querySelector('#title').innerHTML = this.title
  }

}
window.customElements.define('app-component', App)